const {createContainer, asClass, asFunction, asValue} = require('awilix');
const container = createContainer();

const StartUp = require('./startup');
const Server = require('./server');
const Routes = require('./routes');

const { UserController } = require('../api/controllers');
const { UserService } = require('../services');
const { UserRepository } = require('../dal/repositories');
const UserRoutes = require('./routes/user.routes'); 
const User = require('../domain/User');


const config = require('../config/environments');
const db = require('../dal/entities');


container.register({
    app: asClass(StartUp).singleton(),
    server: asClass(Server).singleton()
})
.register(
    {UserController: asClass(UserController).singleton()}
).register(
    {router: asFunction(Routes).singleton()}
).register(
    {config: asValue(config)}
).register(
    {UserRoutes: asFunction(UserRoutes).singleton()}
).register(
    {UserService: asClass(UserService).singleton()}
).register(
    {UserRepository: asClass(UserRepository).singleton()}
).register(
    {db: asValue(db)}
);

module.exports = container;
