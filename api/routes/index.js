const {
    Router
} = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const morgan = require('morgan');

module.exports = function ({UserRoutes}) {
    const router = Router();
    const apiRouter = Router();

    apiRouter.use(cors()).use(bodyParser.json()).use(compression()).use(morgan('dev'));

    apiRouter.use("/user", UserRoutes);
    router.use("/api", apiRouter);

    return router;
}