module.exports = {
    PORT: process.env.PORT,
    DB: {
        username: "postgres",
        password: "secretpassword",
        database: "school_dev",
        host: "localhost",
        dialect: "postgres",
        logging: false
    }
};