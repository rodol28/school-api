const User = require('../User');

module.exports = {
    toDomainEntity(user) {
        const { id, name, lastname } = user;
        return new User({ id, name, lastname });
    }
};
